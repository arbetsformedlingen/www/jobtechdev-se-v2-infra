# jobtechdev-se-infra

Infrastructure scripts for [https://www.jobtechdev.se](https://www.jobtechdev.se) to deploy
the Jobtech website on Kubernetes.

This repository has a companion repository [jobtechdev-se](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se) with code for site and images. You find the documentation in that repository.

The deployment is illustrated in the following images:

![Deployment image](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/raw/main/docs/images/deployment.jpg)


## Installation

This instruction describes how to do the first time installation.

Installation is done via [Aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark).

You need to deploy secrets for backup to S3/minio manually. Use
the file [backup-secrets-example.yaml](backup-secrets-example.yaml) as an example.

You need to deploy secrets mail sending manually. Use
the file [email-secret-example.yaml](email-secret-example.yaml) as an example.

We do not yet have dynamic deployment of EFS volumes. EFS volume is needed to let many pods
access access the same filesystem. Due to the lac of dynamic provisioning of EFS, there are
some issues with permissions. The root volume must be set RW for all.
Therefore first deploy must be done manually.

Create a filesystem to be used for the website following
[instructions](https://gitlab.com/devops-jobtech/calamari-documentation/-/blob/master/peristance.md).

Login to the OpenShift cluster and do the following, depending on cluster.

## Test cluster via Tekton

```shell
oc new-project jobtechdev-se-ENVIRONMENT
kubectl apply -f aardvark-deployer-access-to-project.yaml
# Following line applies the secrets to minio, you must
# have created according to above.
kubectl apply -f backup-secrets.yaml
kubectl apply -k kustomize/overlays/ENVIRONMENT
kubectl apply -f pod-fix-nfs-permissions.yaml
kubectl get pod fix-nfs
# Wait to above command to show status is *completed*
kubectl delete pod fix-nfs
```

## Production clusters with ArgoCD

```shell
oc new-project jobtechdev-se-ENVIRONMENT
# Following line applies the secrets to minio, you must
# have createdit according to above.
kubectl apply -f backup-secrets.yaml
kubectl apply -k kustomize/overlays/ENVIRONMENT
kubectl apply -f pod-fix-nfs-permissions.yaml
kubectl get pod fix-nfs
# Wait to above command to show status is *completed*
kubectl delete pod fix-nfs

# Activate ArgoCD
kubectl apply -f argocd/argocd-deployer-access-to-project.yaml
kubectl apply -f argocd/ENVIROMENT.yaml
```
