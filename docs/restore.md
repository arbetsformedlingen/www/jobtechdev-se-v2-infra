# Restore website


Login to cluster.

Make project where site runs your current.

Get User Id for your project (replace `MY-PROJECT` with the name of your project):
  ```shell
  kubectl get ns MY-PROJECT -o jsonpath="{.metadata.annotations['openshift\.io/sa\.scc\.uid-range']}" \
  | cut -d / -f 1
  ```

Start the restore-helper pod: `kubectl apply -f restore-helper.yaml`

Access the restore helper: `kubectl exec -it restore-helper -- bash`

Do:
```shell
cd /var/www/html
# Delete all files
rm -rf user/* logs/* sessions/*
# Check no files are left, only the three directories.
ls -la logs sessions user

# Copy your backup from minio:
mc cp MINIO/backup/jobtechdev-se/ENVIRONMENT/BACKUP-FILE.tgz backup.tgz
tar xvzf backup.tgz
rm -rf backup.tgz
```

Finally, still in the container, ensure you make all files owned by the User for your project.
Replace `USER_ID` with the User Id you got for your project:
```shell
chown -R USER_ID /var/www/html
exit
```

Back outside your container. List running pods: `kubectl get pods`

Delete all bods starting with *jobtechdev-se-*: `kubectl delete pod NAME`

Wait until the new pod is ready and then verify you can access the site.

Finally delete your restorer-helper container: `kubectl delete pod restore-helper`
